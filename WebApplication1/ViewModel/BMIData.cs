﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {

        [Required(ErrorMessage = "必填欄位")]
        [Range(30,550,ErrorMessage ="30-550")]
        [Display(Name ="體重")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 300, ErrorMessage = "超過30-300")]
        [Display(Name = "身高")]
        public float Height { get; set; }

            public float BMI { get; set; }
            public string Level { get; set; }
        }


    }